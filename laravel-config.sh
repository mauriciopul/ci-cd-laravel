#!/bin/ash
chmod 777 storage
composer install
php artisan key:generate
php artisan migrate:refresh --seed
php artisan passport:install --force
php artisan test
