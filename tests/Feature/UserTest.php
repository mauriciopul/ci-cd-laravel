<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;

class UserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function testCanFindCreateAUser(): void
    {
        $userCreate = User::factory()->create();
        $this->assertDatabaseHas('users', [
            'email' => $userCreate->email,
        ]);
    }
    
    public function testCannotFindCreateAUser(): void
    {
        $userCreate = User::factory()->create();
        $this->assertDatabaseMissing('users', [
            'email' => Str::random(),
        ]);
    }
}
